export const environment = {
  production: true,
  host: 'https://opensegnalazioni-demo.boat.opencontent.io',
  json_server: 'https://api.opencontent.it/belluno',
};
