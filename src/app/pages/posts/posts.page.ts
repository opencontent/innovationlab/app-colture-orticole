import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ReportsService } from '../../services/reports.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit, OnDestroy {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  next: string;
  listPost: any[] = [];
  isLoading: boolean;
  private unsubscribe$ = new Subject<void>();
  imageLoading = true;
  constructor(private serviceReport: ReportsService, private router: Router) {}

  ngOnInit() {
    this.isLoading = true;
    this.serviceReport
      .getMyPosts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.listPost = res.items;
          this.next = res.next;
        },
        (error) => {},
        () => {
          this.isLoading = false;
        }
      );
  }

  loadData(event) {
    setTimeout(() => {
      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.next === null) {
        event.target.disabled = true;
        this.infiniteScroll.disabled = true;
      } else {
        event.target.complete();
        this.serviceReport
          .getReportsByUrl(this.next)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              this.next = res.next;
              this.listPost = [...this.listPost, ...res.items];
            },
            (error) => {},
            () => {
              this.isLoading = false;
            }
          );
      }
    }, 500);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  openDetails(url: string, data) {
    let navigationExtras: NavigationExtras = {
      state: {
        detailPost: data,
      },
    };
    this.router.navigate([url], navigationExtras);
  }

  imageLoaded() {
    this.imageLoading = false;
  }
}
