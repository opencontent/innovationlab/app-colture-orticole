import {Component, ElementRef, NgZone, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {icon, LatLngExpression, Map, Marker, tileLayer, marker, PopupOptions, DomEvent, DomUtil} from 'leaflet';
import {Apollo} from 'apollo-angular';
import {DomSanitizer} from '@angular/platform-browser';
import {LoadingController, ModalController, ToastController} from '@ionic/angular';
import {ZoneService} from '../../services/zone.service';
import {Geolocation, Position} from '@capacitor/geolocation';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import {Control} from 'leaflet';
import * as data from "../../../assets/data.json";


const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = icon({
    iconRetinaUrl,
    iconUrl,
    shadowUrl,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});
Marker.prototype.options.icon = iconDefault;

@Component({
    selector: 'app-map-details',
    templateUrl: './elements-map.page.html',
    styleUrls: ['./elements-map.page.scss'],
})

export class ElementsMapPage implements OnInit, OnDestroy {
    error: any;
    label = 'Seleziona elemento sulla mappa';
    position: Position = null;
    currentPos: GeolocationPosition;
    waitingForCurrentPosition = false;
    @ViewChild('map', {read: ElementRef, static: true}) mapElement: ElementRef;
    @ViewChild('one', {static: false}) d1: ElementRef;
    municipalityData: any;
    private geocode: LatLngExpression = [46.127175, 12.2488552];
    private marker: Marker;
    private mapRef: Map;
    private unsubscribe$ = new Subject<void>();
    private sizeFeatures: any;
    data: any = (data as any).default;
    markers: any;
    center: LatLngExpression;

    constructor(
        private apollo: Apollo,
        public sanitizer: DomSanitizer,
        public modalController: ModalController,
        public ngZone: NgZone,
        private zoneService: ZoneService,
        public loadingController: LoadingController,
        public toastController: ToastController,
    ) {
    }

    ngOnInit() {

        this.sizeFeatures = this.data.features.length
        if (this.mapRef !== undefined) {
            this.mapRef.remove();
        }
        if (this.data.features.length > 0) {
            this.initMap(this.sizeFeatures);
        } else {
            this.error = 'Nessun elemento trovato per questo comune';
        }
    }

    ionViewWillEnter() {
    }

    initMap(sizePoles: any) {

        this.mapRef = L.map(this.mapElement.nativeElement, {
            center: [46.12, 12.24],
            zoom: 8,
            zoomControl: false, tap: false
        }).setView(this.geocode, 8);

        this.mapRef.whenReady(() => this.onMapLoad(this.mapRef))

        const osm = L.tileLayer(
            "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            {
                maxZoom: 19,
                attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            }
        ).addTo(this.mapRef);

        L.control.zoom({position: 'topright'}).addTo(this.mapRef);

        const legend = new Control({position: 'bottomleft'});

        legend.onAdd = (map) => {
            const div = L.DomUtil.create('div', 'info legend');
            div.innerHTML += `<b>Aziende: ${sizePoles || null}</b>`;
            return div;
        };

        legend.addTo(this.mapRef);

        const geojsonMarkerOptions = {
            radius: 8,
            fillColor: "#ff7800",
            color: "#000",
            weight: 1,
            opacity: 1,
            fillOpacity: 0.8
        };

        L.Polygon.addInitHook(function () {
        	this._latlng = this._bounds.getCenter();
        });
        
        L.Polygon.include({
        	getLatLng: function () {
            	return this._latlng;
            },
            setLatLng: function () {} // Dummy method.
        });

        const lightData = L.geoJSON(this.data, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, geojsonMarkerOptions);
            },
            onEachFeature: function (feature, layer) {

                const popupContent =
                    '<h4 class = "text-primary">Fabbricato</h4>' +
                    '<div class="container"><table class="table table-striped">' +
                    "<thead><tr><th>Proprietà</th><th>valori</th></tr></thead>" +
                    "<tbody><tr><td> C.U.A.A. </td><td>" +
                    feature.properties.cuaa +
                    "</td></tr>" 
                    +
                    "<tr><td> Coltura </td><td>" +
                    feature.properties.coltura +
                    "</td></tr>" 
                    +
                    "<tr><td> Identificativo catastale </td><td>" +
                    feature.properties.particelle +
                    "</td></tr>" 
                    +
                    "<tr><td> Comune </td><td>" +
                    feature.properties.text_com +
                    "</td></tr>";

                layer.bindPopup(popupContent, {
                    closeButton: false
                });

            },
        }).addTo(this.mapRef);

        this.markers = L.markerClusterGroup().addLayer(lightData);
        this.mapRef.addLayer(this.markers);
    }

    private onMapLoad(map: L.Map) {
        setTimeout(() => {
            map.invalidateSize(true);
        }, 0);

    }


    centerLeafletMapOnMarker(map, marker) {
        const latLngs = [marker.getLatLng()];
        const markerBounds = L.latLngBounds(latLngs);
        map.fitBounds(markerBounds);
    }


    async requestPermissions() {
        const permResult = await Geolocation.requestPermissions();
        console.log('Perm request result: ', permResult);
    }

    getCurrentPos() {
        try {
            Geolocation.getCurrentPosition().then((coordinates) => {
                this.position = coordinates;
                this.zoneService
                    .addressLookup([this.position.coords.latitude, this.position.coords.longitude])
                    .pipe(takeUntil(this.unsubscribe$))
                    .subscribe(
                        (res) => {
                            const data = res.replace(/cb\(/g, '');
                            const jsonData = JSON.parse(data.replace(/\)/g, ''));
                            this.label = jsonData.display_name;
                            this.geocode = [this.position.coords.latitude, this.position.coords.longitude];
                            this.addCurrentMakerPosition();
                        },
                        (error) => {
                            console.log('err', error);
                        }
                    );
            }).catch(err =>{
                if(err.code === 1){
                    alert('Permessi geocalizazzione disabilitati')
                }else{
                    alert('Geocalizazzione non disponibile')
                }
            });
        } catch (err) {
            console.error('Failed to get current position.', err);
        }
    }

    async presentLoading() {
        const loading = await this.loadingController.create({
            cssClass: 'my-custom-class',
            message: 'Caricamento...',
        });
        await loading.present().then(() => {
            Geolocation.getCurrentPosition({
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0,
            })
                .then((r) => {
                    this.zoneService.addressLookup([r.coords.latitude, r.coords.longitude]).subscribe(
                        (res) => {
                            const data = res.replace(/cb\(/g, '');
                            const jsonData = JSON.parse(data.replace(/\)/g, ''));
                            this.label = jsonData.display_name;
                            this.geocode = [r.coords.latitude, r.coords.longitude];
                            this.addCurrentMakerPosition();
                            loading.dismiss();
                        },
                        (error) => {
                            loading.dismiss();
                        },
                        () => {
                            loading.dismiss();
                        }
                    );
                    loading.dismiss();
                })
                .catch((err) => {
                    loading.dismiss();
                    this.presentToastWithOptions();
                });
        });

        const {role, data} = await loading.onDidDismiss();
        console.log('Loading dismissed!');
    }

    async presentToastWithOptions() {
        const toast = await this.toastController.create({
            header: 'Geocalizazzione non abilitata',
            message: '',
            position: 'top',
            buttons: [
                {
                    text: 'Chiudi',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    },
                },
            ],
        });
        await toast.present();

        const {role} = await toast.onDidDismiss();
        console.log('onDidDismiss resolved with role', role);
    }

    ngOnDestroy(): void {
        // destroy instance map
        setTimeout(_ => {
            this.mapRef.off();
            this.mapRef.remove();
        }, 200)

        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    addCurrentMakerPosition() {
        const currentPositionMarkerIcon = icon({
            iconUrl: 'assets/icon/marker.png',
            iconSize: [20, 60],
            popupAnchor: [0, -20],
        });

        this.marker = marker(this.geocode, {icon: currentPositionMarkerIcon, draggable: false})
            .bindPopup(`<b>${this.label}</b>`, {closeButton: false})
            .addTo(this.mapRef);

        this.centerLeafletMapOnMarker(this.mapRef, this.marker)
    }
}
