import { Component } from '@angular/core';

import { ModalController, NavController, Platform } from '@ionic/angular';
import { Apollo, gql } from 'apollo-angular';
import { MunicipalitiesPage } from './modals/municipalities/municipalities.page';
import { SplashScreen } from '@capacitor/splash-screen';
import { StorageService } from './services/storage.service';
import { ZoneService } from './services/zone.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  private comuni: any;
  constructor(
    private platform: Platform,
    public modalController: ModalController,
    private navController: NavController,
    private apollo: Apollo,
    private storage: StorageService,
    private zoneService: ZoneService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      await SplashScreen.show({
        showDuration: 4000,
        autoHide: true,
      });
      await this.storage.getMunicipality().then((res) => {
        if (res == null) {
          this.presentModal();
        }
      });
    });
  }

  async presentModal() {
    this.zoneService.getMunicipalities()
      .subscribe(async res => {
        this.comuni = res

        const modal = await this.modalController.create({
          component: MunicipalitiesPage,
          cssClass: 'my-custom-class',
          swipeToClose: true,
          componentProps: {
            municipality: this.comuni,
          },
        });

        modal.onDidDismiss().then((data) => {
          window.location.reload();
        });
        return await modal.present();
      });
  }
}
