import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FullscreenImagePage } from './fullscreen-image.page';

describe('FullscreenImagePage', () => {
  let component: FullscreenImagePage;
  let fixture: ComponentFixture<FullscreenImagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullscreenImagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FullscreenImagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
